import java.util.Scanner;

public class GameLauncher{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		//Start by asking the player which game they want to play:
		System.out.println("Type 1 to play Hangman. Type 2 to play Wordle");
		
		int pickedNumber = reader.nextInt();
		
		//Starting Hangman game
		if(pickedNumber == 1){
			System.out.println("Starting Hangman...");
			Hangman();
		}
		//Starting Wordle game
		else if(pickedNumber == 2){
			System.out.println("Starting Wordle...");
			Wordle();
		}
		//User is not very smart and they typed a number that was not 1 or 2
		else{
			System.out.println("Please enter a valid number");
		}
		
	}
	public static void Hangman(){ 
		Scanner reader = new Scanner(System.in);
		System.out.println("type a 4 word");
		String userLetter= reader.nextLine();
		userLetter = userLetter.toUpperCase();
		
		Hangman.runGame(userLetter);
		
		System.out.println("wanna play again? type 1");
		System.out.println("wanna play Wordle? type 2");
		System.out.println("want to exit? type whatever number");
		
		
		/*Once the game is completed, we shall ask the user if they want to 
		* play again or play another game or leave
		*/
		int typedNumber = reader.nextInt();
		
		if(typedNumber == 1){
			System.out.println("Restarting Hangman...");
			Hangman();
		}
		else if(typedNumber == 2){
			System.out.println("Starting Wordle...");
			Wordle();
		}
		else{
			System.out.println("Closing program...");
			reader.close();
		}
	}
	public static void Wordle(){
		Scanner reader = new Scanner(System.in);
		
		Wordle.runGame();
		
		/*Once the game is completed, we shall ask the user if they want to 
		* play again or play another game or leave
		*/
		System.out.println("wanna play again? type 2");
		System.out.println("wanna play hangman? type 1");
		System.out.println("want to exit? type whatever number");
		
		int typedNumber = reader.nextInt();
		
		if(typedNumber == 1){
			System.out.println("Starting Hangman...");
			Hangman();
		}
		else if(typedNumber == 2){
			System.out.println("Restarting Wordle...");
			Wordle();
		}
		else{
			System.out.println("Closing program...");
			reader.close();
		}
   }
}