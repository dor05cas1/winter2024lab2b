import java.util.Random;
import java.util.Scanner;

public class Wordle{
  public static void main(String[] args){

  //if i already have generateWord called in runGame, i dont think I have to call it here. it works this way too.  (ew its supposed to be a shrug emoji)
  runGame();



  }
  public static String generateWord() {
  //class notes on random
    Random rando = new Random();

    String[] wordArray = new String [] {"DREAM", "BRICK", "WALTZ", "QUACK", "FUZIL", "CRAZY", "COZEY", "KLUTZ", "CHYME", "SAUCE", "SALTY", "QUICK", "JUMBO", "ALONE", "BORED", "CLYDE", "PUNKS", "MIROH", "HEAVY", "LUCKY", "MIXED", "RULES", "WORLD"};
    int x = rando.nextInt(wordArray.length);

    return wordArray[x];

  }
  public static boolean letterinWord(String word, char letter){

    for(int i = 0; i < word.length(); i++){
      if(word.charAt(i) == letter){
        return true;
      }
    }
    return false;

  }
  public static boolean letterinSlot(String word, char letter, int position){

    if(word.charAt(position) == letter){
      return true;
    }
    else{
      return false;
    }

  }
  public static String [] guessWord(String answer, String guess){
  //start by making a string array
  //initialize every element to white
    String [] colors = new String [] {"white", "white", "white", "white", "white"};

    for(int i= 0; i < answer.length(); i++ ){
      char guessChar = guess.charAt(i);
    //check letterinword method > yellow
      if(letterinWord(answer, guessChar) == true){
        colors[i] = "yellow";
      }
    //check letterinslot > green
      if(letterinSlot(answer, guessChar, i) == true){
        colors[i] = "green";

      }
    }
    return colors;

  }
  public static String presentResults(String word, String [] colors){
    //this is the part we add the final strings w the colours...
  final String white = "\u001B[0m";
  final String green = "\u001B[32m";
  final String yellow = "\u001B[33m";

  String userGuess = "";

  for(int i = 0; i < colors.length; i++){
    switch (colors[i]){
    case "white":
    //reset to white at the end for each case and add breaks
      userGuess += white + word.charAt(i) + white;
    break;
      case "yellow":
      userGuess += yellow + word.charAt(i) + white;
    break;
    case "green":
      userGuess += green + word.charAt(i) + white;
    break;
    }
  }
  return userGuess;

  }
  public static String readGuess() {
    //when not length 5, ask to enter another word that is length 5
    //toUppercase()
    Scanner reader = new Scanner(System.in);
    String wordleGuess;

    System.out.println("Make a 5-letter word guess: ");
    wordleGuess = reader.nextLine();

    if (wordleGuess.length() != 5) {
    
      System.out.println("Please enter a valid 5-letter word.");
      wordleGuess = reader.nextLine();
      wordleGuess = wordleGuess.toUpperCase();
    } else {
      wordleGuess = wordleGuess.toUpperCase();
    }

    return wordleGuess;
  }

  public static void runGame() {
    //make an int so that it stops running when no more tries
    //do a while loop that scans as long as tries is not 0 and the word is not guessed yet
    //call readguess, guessword and presentResults
    //NO RETURNS
    int tries = 6;
    String targetWord = generateWord();
    boolean correctGuess = false;

    while (tries != 0 && !correctGuess) {
      String userGuess = readGuess();

      System.out.println(presentResults(userGuess, guessWord(targetWord, userGuess)));
      tries--;

      if (targetWord.equals(userGuess)) {
        correctGuess = true;
        System.out.println("You win! You had " + (1 + tries) + " tries left.");
    //1 + tries just so that it doesnt remove a try when u do get it right
      } else {
        System.out.println("Try again. You have " + tries + " tries left");
      }
    }
  //u lost statement at the end
    if (!correctGuess) {
      System.out.println("Maximum tries exceeded. The correct word was: " + targetWord);
    }
  }

}