import java.util.Scanner;

//write down the skeleton of script
public class Hangman {
	public static void main(String [] args){ 
		Scanner reader = new Scanner(System.in);
		System.out.println("type a 4 word");
		String userLetter= reader.nextLine();
		userLetter = userLetter.toUpperCase();
		
		runGame(userLetter);
		//reader.close();
	}
	public static int isLetterinWord(String word, char c){
		//return position of letter that is in word
		for(int i=0; i < word.length(); i++){
			if(word.charAt(i) == c){
				//checks if the character entered by user is in the word
				return i;
				//returns position (an int)
			}
		}
		return -1;
	}
	public static char toUpperCase(char c){
		//a -> A
		//A -> A
		//very short	
		return Character.toUpperCase(c);
	}
	public static void printWork(String word, boolean[] letters) {
		//update for lab: changed the letter booleans into an array
		String guessLetter = "";

		for (int i = 0; i < letters.length; i++) {
			//if the user guesses the letter incorrectly
			if (!letters[i]) {
				//the string guessLetter is updated to _
				guessLetter += "_";
			//if its correct
			} else {
				//the string guessLetter is updated to the correct char, at the right position
				guessLetter += word.charAt(i);
			}
		}
		//print the result
		System.out.println(guessLetter);
	}

	public static void runGame(String word){
		//a boolean array that represent the letters in 4 letter word
		boolean [] letters = new boolean [4];
		int triesLeft = 6;
		char myChar ='_';
		int checkGuess = 0;
		//while tries remaining are still over 0
		while(triesLeft > 0 && !(letters[0] && letters[1] && letters[2] && letters[3])){
			
			Scanner reader = new Scanner(System.in);

			System.out.print("Make a letter guess: ");
			//makes sure it only takes first char + uppercase
			myChar = toUpperCase(reader.next().charAt(0));
			checkGuess = isLetterinWord(word, myChar);
			//if letter is not in word, we returned -1, so here, we should check if its -1 or not
			if (checkGuess == -1){
				//number of tries left reduced by one
				triesLeft --;
				System.out.println("YOU MISSED...");
			}
			else{
				letters[checkGuess] = true;
				System.out.println("yay");
			}
			printWork(word, letters);
			System.out.println("tries left: " + triesLeft);
		}
		//if all letters are false after loop...
		if (!(letters[0] && letters[1] && letters[2] && letters[3])){
			System.out.println("You lost, the word was: " + word);
			
		}else{
		//if letters are true after loop...
			System.out.println("you won... the word is: " + word);
		}
	}
	
}
